// THÊM SINH VIÊN
var dssv = [];

const DSSV_LOCAL = "DSSV_LOCAL";
//khi user load trang => lấy dữ liệu từ localStorage
var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  dssv = JSON.parse(jsonData);
  renderDSSV(dssv);
}
console.log("jsonData: ", jsonData);

function themSV() {
  var sv = layThongTinTuForm();

  // push(): thêm phần tử vào array
  dssv.push(sv);
  // convert data
  let dataJson = JSON.stringify(dssv);
  // lưu vào localStorage
  localStorage.setItem("DSSV_LOCAL", dataJson);

  // render dssv lên table
  renderDSSV(dssv);
  resetForm();
  // tbodySinhVien
  // var contentHTML = "";
  // for (var i = 0; i < dssv.length; i++) {
  //   var item = dssv[i];
  //   var contentTr = `
  //   <tr>
  //   <td> ${item.ma}</td>
  //   <td> ${item.ten}</td>
  //   <td> ${item.email}</td>
  //   <td>0</td>
  //   <td>
  //   <button onclick="xoaSV('${item.ma}')" class='btn btn-danger'> Xóa </button>
  //   </td>

  //   </tr>
  //   `;
  //   contentHTML += contentTr;
  // }
  // document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xoaSV(id) {
  console.log("id: ", id);
  //splice: cut, slice:copy
  console.log("dssv sun", dssv);
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    //splice(viTri, số lượng)

    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
}
function suaSV(id) {
  console.log("id: ", id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  console.log("viTri: ", viTri);
  // Show thông tin lên form
  var sv = dssv[viTri];

  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function capNhatSV() {
  //layThongTinTuForm() => return object sv
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dssv[viTri] = sv;
  renderDSSV(dssv);
}

//RETSET FORM SAU KHI UPDATE
function resetForm() {
  document.getElementById("formQLSV").reset();
}

// <!-- splice, findIndex, map, push, forEach-->
// <!--localStorage: lưu trữ, JSON: convert data-->
